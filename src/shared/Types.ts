export type System = {
  id:string;
  title:string;
  description:string;
  status:string;
  date:string;
}
